<?php
/**
 * @file
 * Render the static google map image formatter.
 */
?>

<?php if ($display_link): ?>
  <a href="<?php print $url ?>" target="_blank">
<?php endif; ?>
  <picture>
    <img src="<?php print $settings['url_google'] . $settings['url_image'] ?>">
  </picture>
<?php if ($display_link): ?>
  </a>
<?php endif; ?>
