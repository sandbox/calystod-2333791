<?php
/**
 * @file
 * Render responsive static google map images formatter.
 */
?>

  <?php if ($display_link): ?>
    <a href="<?php print $url ?>" target="_blank">
  <?php endif; ?>

  <picture>

  <?php foreach($settings['static_map_breakpoints'] as &$breakpoint) : ?>
    <source srcset="<?php print $settings['url_google'] . $breakpoint['url_image'] ?>" media="<?php print $breakpoint['breakpoint']; ?>" >
  <?php endforeach; ?>

  <img srcset="<?php print $settings['url_google'] . $breakpoint['url_image'] ?>" >

  </picture>

  <?php if ($display_link): ?>
    </a>
  <?php endif; ?>
