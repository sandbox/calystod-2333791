CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
The module Google Static Map adds two formatters for geofield which generate a
Google static map, an image more light than a Google map :
* A basic formatter. You indicate just the width, the height and the zoom of
  the map
* If Picture is enable, a responsive formatter which generate differents images
  for each breakpoints of your choose.

You can add a link on the image with the module to send on Google Map.

REQUIREMENTS
------------
This module requires the following modules:
* Geofield (https://drupal.org/project/geofield)
* Geocoder (https://www.drupal.org/project/geocoder)

RECOMMENDED MODULES
------------
* Picture (https://drupal.org/project/picture) :
  When it's enabled, the responsive formatter is available. You must configure
  your breakpoints.

* Addressfield (https://www.drupal.org/project/addressfield) :
  To generate the link on the image with address instead of geolocation.

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Configure formatter in manage display page of content types: 
  - Choose Google Static Map or Responsive Google Static Map format if
    available.
* Configure formatter in settings of field in views:
  - Choose Google Static Map or Responsive Google Static Map formatter if
    available.

MAINTAINERS
-----------
Current maintainers:
* Claire Desbois (calystod) - https://www.drupal.org/user/1283074
* Pierre Buyle (mongolito404) - https://www.drupal.org/u/mongolito404
This project has been sponsored by:
* Pheromone
  Phéromone is a creative and digital business that creates their own products
  and platforms and that provides custom
  Web strategy, design and technology to selected partners, from Canadian icons
  to internationally renowned institutions.
